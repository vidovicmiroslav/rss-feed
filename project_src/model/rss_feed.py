import feedparser


class RssFeed(object):
    def __init__(self, url):
        self.__url = url

    def parse_data(self):
        data = feedparser.parse(self.__url)
        return data.entries
    